# GODDeSS

GODDeSS (**G**-eant4 **O**-bjects for **D**-etailed **De**-tectors with **S**-cintillators and **S**-iPMs) is a Geant4-based package extending Geant4 by some useful object classes for scintillator tiles, optical
fibres and photon detectors.

Find the tarball with the latest version of GODDeSS in this repository.

For more information, consult the [Wiki](https://git.rwth-aachen.de/3pia/forge/goddess-package/wikis/home).